import {MyBigNumber} from '../models/NumberModel.js'

export const addNumberResult = async (req, res)=>{
    try {
        const newResult = req.body
        const result = new MyBigNumber(newResult)
        await result.save()
        res.status(200).json(result)
    } catch (error) {
        console.log("Xảy ra lỗi khi lưu thông tin", error);
        res.status(500).json({error: error})
    }
}

export const getResult = async (req, res)=>{
    try {
        const result = await MyBigNumber.find()
        res.status(200).json(result)
    } catch (error) {
        console.log("Có lỗi khi lấy data", error);
        res.status(500).json({error: error})
    }
}