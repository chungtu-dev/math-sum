import express from 'express';
import {addNumberResult, getResult} from '../controllers/Math.js'

const router = express.Router()

router.post('/', addNumberResult)

router.get('/', getResult)

export default router