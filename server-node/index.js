import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import mathRoute from './routes/router.js'

dotenv.config()

const app = express()

app.use(cors())
app.use(express.json({ limit: '30mb' }))

app.use('/data', mathRoute)

const PORT = process.env.PORT || 5000

const URL_DB = process.env.LOCALHOST_CLIENT_SERVER || ""
// const URL_DB_MONGO = process.env.MONGO_DB || ""

const connect = () => {
    try {
        // mongoose.connect('mongodb://127.0.0.1:1200/data');
        mongoose.connect(URL_DB)
        console.log("Connected to db");
    } catch (error) {
        console.log('cannot connect server data', error);
    }
};

app.listen(PORT, () => {
    connect();
    console.log(`connected to Backend, port - ${PORT}`);
});