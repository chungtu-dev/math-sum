import mongoose from "mongoose";
const schema = new mongoose.Schema({
    number1: {
        type: String,
        required: true
    },
    number2: {
        type: String,
        required: true
    },
    result: {
        type: Number,
        required: false
    }
}, { timestamps: true })

export const MyBigNumber = mongoose.model("NumberModel", schema)