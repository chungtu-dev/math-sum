import axios from 'axios';

// const API_URL = "http://localhost:5000"
const API_URL = "http://localhost:1200"

export const postNewMathReq = async (data) => {
    try {
        await axios.post(`${API_URL}/data`, data)
        console.log("data", data);
    } catch (error) {
        console.log("Lỗi cập nhật data",error);
    }
}