import React, { useState } from 'react'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
// import axios from 'axios'
import { postNewMathReq } from '../api/index'

const Home = () => {

    const [state, setState] = useState({
        number1: "",
        number2: "",
    })
    const [result, setResult] = useState("")

    const handleResult = async (e) => {
        e.preventDefault()
        try {

            const resultMath = Number(state.number1) + Number(state.number2)
            // console.log(Number(resultMath));
            setResult(Number(resultMath))

            const data = {
                // ...state,
                number1: Number(state.number1),
                number2: Number(state.number2),
                result: resultMath
            }
            postNewMathReq(data)
        } catch (error) {
            console.log('Lỗi', error);
        }
    }

    const setData = (e) => {
        // console.log(e.target.value);
        const regex = /^[0-9\b]+$/;
        if (e.target.value === "" || regex.test(e.target.value)) {
            const { name, value } = e.target;
            setState((preval) => {
                return {
                    ...preval,
                    [name]: value
                }
            })
        }
    }

    return (
        <div className="container_form">
            <Form onSubmit={handleResult}>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label style={{color: 'white'}}>Number 1</Form.Label>
                    <Form.Control name="number1" min="0" type="number" placeholder="Number 1" onChange={setData} value={state.number1} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label style={{color: 'white'}}>Number 2</Form.Label>
                    <Form.Control name="number2" min="0" type="number" placeholder="Number 2" onChange={setData} value={state.number2} />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Kết quả
                </Button>

                <div>
                    {
                        result && (
                            <>
                                {[
                                    'success',
                                ].map((variant) => (
                                    <Alert key={variant} variant={variant} className="mt-3">
                                        Tổng {state.number1} và {state.number2} = {result}
                                    </Alert>
                                ))}
                            </>
                        )
                    }
                </div>
            </Form>
        </div>
    )
}

export default Home