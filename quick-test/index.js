var prompt = require('prompt');
function add() {

    prompt.start();
    prompt.get(["num1", "num2"],
        function (err, res) {
            if (err) {
                console.log(err);
            } else {
                var sum = parseFloat(res.num1)
                    + parseFloat(res.num2);

                // Print the sum
                console.log("Tổng của " + res.num1
                    + " và " + res.num2 + " là " + sum);
            }
        });
}

// Calling add function
add();

