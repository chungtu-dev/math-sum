# This project using `2 SERVER` and `1 CLIENT`

### `npm start` to run application

**Note: server to connect**
To run client UI
- `client/src/api` : client connect api
1. http://localhost:5000 : this server is connect to `MongoDB`
2. http://localhost:1200 : this server is connect to `JSON-SERVER`

## SERVER 1: JSON-SERVER
### `npm run client-server`
- `client/client-server`: server json-server
All data C-R-U-D save into `db.json`

## SERVER 2: MONGO DB
- `client/server-node`

On `index.js`
- `const URL_DB_MONGO` : connect to `MongoDB`, config URL to connect in file `.env`

Similar:
- `const URL_DB` : try connect to server on `LOCAL SERVER` (example `JSON-SERVER`)


## QUICK TEST
- `client/quick-test`
Quick test on terminal
To run quick test: `node index.js`. Just input 2 number (to SUM) and see the result